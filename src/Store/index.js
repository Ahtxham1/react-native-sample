import {createStore} from 'redux';

const initialState = {
  UserLoginInfo: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'USER_DETAIL': {
      return {UserLoginInfo: action.userDetail};
    }
  }
  return state;
};

export const Store = createStore(reducer);
