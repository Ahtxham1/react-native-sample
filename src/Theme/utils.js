import moment from 'moment';
import SocketIOClient from 'socket.io-client';
import {Constants} from '../Theme';

export const emailValidator = email => {
  // const re = /\S+@\S+\.\S+/;
  let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
  if (!email || email.length <= 0) {
    return 'Email field cannot be empty.';
  }
  if (!re.test(email)) {
    return 'Oops! We need a valid email address.';
  }

  return '';
};

export const TextValidator = text => {
  const rex = /^[a-zA-Z ]+$/;
  if (!text || text.length <= 0) {
    return ' cannot be empty.';
  }
  if (text.length < 6) {
    return ' cannot be less the 6 characters.';
  }
  if (!rex.test(text)) {
    return '  field contains only characters ';
  }
  return '';
};

export const NumberValidator = CNIC => {
  if (!CNIC || CNIC.length <= 0) {
    return ' cannot be empty.';
  }
  if (isNaN(CNIC)) {
    return ' Should be in number format';
  }
  return '';
};

export const passwordValidator = text => {
  const letters = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  if (!text || text.length <= 0) {
    return 'Password cannot be empty.';
  }
  if (!text || text.length <= 6) {
    return 'Password cannot be less then 6 character.';
  }
  if (!letters.test(text)) {
    return 'Password must contain a special characters and a number ';
  }
  return '';
};

export const convertTime12to24 = time12h => {
  const [time, modifier] = time12h.split(' ');
  let [hours, minutes] = time.split(':');
  if (hours === '12') {
    hours = '00';
  }
  if (modifier === 'PM') {
    hours = parseInt(hours, 10) + 12;
  }
  return `${hours}:${minutes}`;
};

export const timeConversion = time => {
  if (moment(time).isValid(time)) {
    return moment(time).format('hh:mm A');
  } else {
    let t = new Date('2021-01-01T' + time + '.000Z');
    return moment.utc(t).format('hh:mm A');
  }
};

export const SocketClient = () => {
  return SocketIOClient(Constants.SOCKET, {
    reconnection: true,
    reconnectionDelay: 500,
    reconnectionAttempts: Infinity,
    transports: ['websocket'],
  });
};
