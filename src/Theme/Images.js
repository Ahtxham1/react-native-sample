const images = {
  SPLASH: require('./Images/Splash.png'),
  SPLASH_LOGO: require('./Images/SplashLogo.png'),
};

export default images;
