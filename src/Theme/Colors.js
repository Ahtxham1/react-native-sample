const colors = {
  themeColor: '#16baff',
  themeGrey: '#3D3D3D',
  themeGreyNew: '#303030',
  themeLightGrey: '#bfbfbf',
  availability: '#4DFF00',
  themeGreyText: '#7d7d7d',
  white: 'white',
  block: 'grey',
  Red: 'red',
  BlackOpacity_4: 'rgba(0,0,0,0.4)',
  HighLight: 'rgba(0,0,0,0)',
  DividingLine: '#A6A6A6',
  warningColor: '#ff6745',
  seprator: '#e4e4e4',
  SepratorLine: '#e7e7e7',
};
export default colors;