import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './style';

const ImageButton = ({...props}) => (
  <TouchableOpacity
    onPress={props.buttonOnPress}
    style={styles.buttonContainer}>
    <Text style={styles.buttonText}>{props.buttonText}</Text>
  </TouchableOpacity>
);

export default ImageButton;
