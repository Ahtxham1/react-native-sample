import {StyleSheet} from 'react-native';
import {Colors} from '../../Theme/';

const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    backgroundColor: Colors.themeColor,
  },

  buttonText: {
    color: 'white',
    fontSize: 15,
    alignSelf: 'center',
    padding: 10,
  },
});

export default styles;
