import {StyleSheet} from 'react-native';

import {Colors} from '../../Theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  mainContent: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    flexDirection: 'column',
  },

  logoImage: {
    resizeMode: 'contain',
    width: '70%',
    height: 150,
    alignSelf: 'center',
  },

  credentials: {
    width: '80%',
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: Colors.white,
  },

  inputField: {},

  inputFieldPlaceholder: {
    color: Colors.white,
    opacity: 0.2,
  },

  textStyle: {
    color: Colors.themeColor,
    fontSize: 16,
    alignSelf: 'center',
    margin: 7,
  },

  errorText: {
    color: Colors.Red,
    margin: 7,
  },

  buttonStyle: {
    backgroundColor: Colors.themeColor,
    height: 36,
  },

  buttonText: {
    color: Colors.white,
    fontSize: 16,
  },
});

export default styles;
