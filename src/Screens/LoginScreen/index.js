import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {Button, Container, Content, Item, Input, Label} from 'native-base';

import {connect} from 'react-redux';

import {useTranslation} from '../../LanguageContext';
import {Colors} from '../../Theme';
import {NavigationHeader} from '../../Components';
import styles from './style';

const LoginScreen = props => {
  const [userDetail, setUserDetail] = useState({});
  const Language = useTranslation();

  const login = () => {
    // console.log(userDetail);
    props.dispatch({type: 'USER_DETAIL', userDetail});
    props.navigation.navigate('HomeScreen');
  };

  const textOnchange = (text, name) => {
    let user = userDetail;
    user[name] = text;
    setUserDetail(user);
  };

  return (
    <Container style={styles.container}>
      <NavigationHeader
        HeaderText={Language.LoginScreenHeaderText}
        LeftOnPress={() => props.navigation.navigate('HomeScreen')}
        LeftText={'Cancel'}
        // RightImageSrc={Images.WavesLogo}
      />

      <Content contentContainerStyle={styles.mainContent}>
        {/* <Image style={styles.logoImage} source={Images.LoginLogo} /> */}

        <View style={styles.credentials}>
          <Item floatingLabel>
            <Label>Email</Label>
            <Input
              style={styles.inputField}
              placeholder={'Email'}
              keyboardType="email-address"
              disableFullscreenUI={true}
              placeholderTextColor={Colors.themeGreyText}
              autoCapitalize="none"
              secureTextEntry={false}
              placeholderStyle={styles.inputFieldPlaceholder}
              value={userDetail.password}
              onChangeText={text => textOnchange(text, 'email')}
            />
          </Item>
          <Item floatingLabel>
            <Label>Password</Label>
            <Input
              style={styles.inputField}
              placeholder={'Password'}
              disableFullscreenUI={true}
              placeholderTextColor={Colors.themeGreyText}
              autoCapitalize="none"
              secureTextEntry={true}
              placeholderStyle={styles.inputFieldPlaceholder}
              value={userDetail.email}
              onChangeText={text => textOnchange(text, 'password')}
            />
          </Item>

          {/* <TouchableWithoutFeedback>
            <View block style={{height: 42}}>
              <Text
                style={styles.textStyle}
                onPress={() => this._forgetPassword()}>
                Forgot Password?{' '}
              </Text>
            </View>
          </TouchableWithoutFeedback>
           */}
          <Button block info style={styles.buttonStyle} onPress={() => login()}>
            <Text style={styles.buttonText}>Login</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

// const mapStateToProps = state => {
//   return {Counter: state.counter};
// };

// const mapDispatchToProps = dispatch => {
//   return {
//     Login: () => dispatch({type: 'LOGIN'}),
//   };
// };

export default connect()(LoginScreen);
