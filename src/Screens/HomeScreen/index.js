import React from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';

import {useTranslation} from '../../LanguageContext';
import {Button, NavigationHeader} from '../../Components';
import styles from './style';

const HomeScreen = props => {
  const Language = useTranslation();

  return (
    <>
      <NavigationHeader
        HeaderText={Language.HomeScreenHeaderText}
        LeftOnPress={() => props.navigation.navigate('LoginScreen')}
        LeftText={'Cancel'}
        // RightImageSrc={Images.WavesLogo}
      />
      <View style={styles.mainConstainer}>
        <Text>{Language.HomeScreen}</Text>
        <Text>{props.UserLoginInfo.email}</Text>
        <Button
          buttonText={Language.LoginScreen}
          buttonOnPress={() => props.navigation.navigate('LoginScreen')}
        />
      </View>
    </>
  );
};

const mapStateToProps = state => {
  return {UserLoginInfo: state.UserLoginInfo};
};

export default connect(mapStateToProps)(HomeScreen);
