import React from 'react';
import {View, Text, ImageBackground} from 'react-native';

import {Images} from '../../Theme';
import styles from './style';

const SplashScreen = props => {
  React.useEffect(() => {
    const timeout = setTimeout(() => {
      props.navigation.navigate('LoginScreen');
    }, 3000);
    return () => {
      timeout.clear();
    };
  }, [props.navigation]);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={Images.SPLASH}
        style={styles.splashBackgoundImage}>
        <Text style={styles.text}>Logo</Text>
      </ImageBackground>
    </View>
  );
};

export default SplashScreen;
